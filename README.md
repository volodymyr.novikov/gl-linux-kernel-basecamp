# Repository for GL Linux kernel exercises and tasks

## exercise01
Bash script for deletion of files which starts with -/_/~ or ends with .tmp

## exercise02
Bash script for appending '~' to files which 30 days old

## task01
Simple file manager

## task02
Bash script with a guess number game